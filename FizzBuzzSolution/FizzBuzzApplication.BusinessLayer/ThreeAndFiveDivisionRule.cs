﻿//---------------------------------------------------------------------------------------------------------
// <copyright file="ThreeAndFiveDivisionRule.cs" company="TCS">
//  Copyright (c) TCS.  All rights reserved.
// </copyright>
//
////<summary>
//  Divisible by three and five functionalities.
// </summary>
//---------------------------------------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer
{
    using System;
    using System.Globalization;
    /// <summary>
    /// class used for 3 and 5 division process
    /// </summary>
    public class ThreeAndFiveDivisionRule : IDivisionRule
    {
        /// <summary>
        /// Division process to check if the number is divisible by both 3 and 5
        /// </summary>
        /// <param name="number">number</param>
        /// <param name="day">day of the week to handle wizz & wuzz condition</param>
        /// <returns></returns>
        public string DivisionProcess(int number, DayOfWeek day)
        {
            if (day != DayOfWeek.Wednesday)
                return number % 15 == 0 ? FizzBuzzResources.Fizz + " " + FizzBuzzResources.Buzz : number.ToString(CultureInfo.CurrentCulture);
            else
                return number % 15 == 0 ? FizzBuzzResources.Wizz + " " + FizzBuzzResources.Wuzz : number.ToString(CultureInfo.CurrentCulture);
        }
    }
}