﻿// ---------------------------------------------------------------------------
// <copyright file="ThreeDivisionRule.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
////<summary>
//  Divisible by Three functionalities.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer
{
    using System;
    using System.Globalization;
    /// <summary>
    /// Interface used for 3 divison process
    /// </summary>
    public class ThreeDivisionRule : IDivisionRule
    {
        /// <summary>
        /// Division process to check if the number is divisible 3
        /// </summary>
        /// <param name="number">number</param>
        /// <param name="day">day of the week to handle wizz & wuzz condition</param>
        /// <returns></returns>
        public string DivisionProcess(int number, DayOfWeek day)
        {
            if (day != DayOfWeek.Wednesday)
                return number % 3 == 0 ? FizzBuzzResources.Fizz : number.ToString(CultureInfo.CurrentCulture);
            else
                return number % 3 == 0 ? FizzBuzzResources.Wizz : number.ToString(CultureInfo.CurrentCulture);
        }
    }
}