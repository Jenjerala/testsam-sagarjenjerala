﻿// ---------------------------------------------------------------------------
// <copyright file="IBusinessLayer.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        IBusinessLayer interface for main BusinessLayer.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer
{
    using System;
    using System.Collections.Generic;
    /// <summary>
    /// interface for repository
    /// </summary>
    public interface IBusinessLayer
    {
        /// <summary>
        /// Gets or sets Property to set Current Day of the week.
        /// </summary>
        DayOfWeek CurrentDay { get; set; }
       
        /// <summary>
        /// generates fizzbuzz sequence
        /// </summary>
        /// <param name="inputNumber">user input</param>
        /// <returns>FizzBuzz entity</returns>
        List<string> GenerateFizzBuzzSequence(int inputNumber);
    }
}