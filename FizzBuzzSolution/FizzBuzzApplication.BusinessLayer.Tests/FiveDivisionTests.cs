﻿// ---------------------------------------------------------------------------
// <copyright file="FiveDivisionTests.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains the test cases for Five Divisibles.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer.Tests
{
    using System;
    using FizzBuzzApplication.BusinessLayer;
    using NUnit.Framework;
    /// <summary>
    /// test cases for five divible numbers.
    /// </summary>
    public class FiveDivisionTests
    {
        /// <summary>
        /// Local Variable for Five Division.
        /// </summary>
        private FiveDivisionRule fiveDivision;

        /// <summary>
        /// To Initialize the objects used in the test cases.
        /// </summary>
        [SetUp]
        public void Initialise()
        {
            this.fiveDivision = new FiveDivisionRule();
        }

        /// <summary>
        /// Test Case for Five Divison. Pass -- Monday
        /// </summary>
        [Test]
        public void Test_FiveDivisible_5_Monday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Buzz, this.fiveDivision.DivisionProcess(5, DayOfWeek.Monday));
        }

        /// <summary>
        /// Test Case for Five Divisible. Pass -- Wednesday
        /// </summary>
        [Test]
        public void Test_FiveDivisible_5_Wednesday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Wuzz, this.fiveDivision.DivisionProcess(5, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Five Division. Fail -- Monday  
        /// </summary>
        [Test]
        public void Test_FiveDivision_5_Monday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Wuzz, this.fiveDivision.DivisionProcess(5, DayOfWeek.Monday));
        }

        /// <summary>
        /// Test Case for Five Division. Fail -- Wednesday
        /// </summary>
        [Test]
        public void Test_FiveDivision_5_Wednesday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Buzz, this.fiveDivision.DivisionProcess(5, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Five Division. Pass -- Monday
        /// </summary>
        [Test]
        public void Test_FiveDivision_6_Monday_Pass()
        {
            Assert.AreEqual("6", this.fiveDivision.DivisionProcess(6, DayOfWeek.Monday));
        }

        /// <summary>
        /// Test Case for Five Division. Pass -- Wednesday
        /// </summary>
        [Test]
        public void Test_FiveDivision_6_Wednesday_Pass()
        {
            Assert.AreEqual("6", this.fiveDivision.DivisionProcess(6, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Five Division. Fail -- Monday
        /// </summary>
        [Test]
        public void Test_FiveDivision_6_Monday_Fail()
        {
            Assert.AreNotEqual("6", this.fiveDivision.DivisionProcess(7, DayOfWeek.Monday));
        }

        /// <summary>
        /// Test Case for Five Division. Fail -- Wednesday
        /// </summary>
        [Test]
        public void Test_FiveDivision_6_Wednesday_Fail()
        {
            Assert.AreNotEqual("6", this.fiveDivision.DivisionProcess(7, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// To Dispose the used Objects.
        /// </summary>
        [TearDown]
        public void Dispose()
        {
            this.fiveDivision = null;
        }
    }
}
