﻿// ---------------------------------------------------------------------------
// <copyright file="ThreeDivisionTests.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains the test cases for Three Divisibles.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer.Tests
{
    using System;
    using NUnit.Framework;
    using FizzBuzzApplication.BusinessLayer;
    /// <summary>
    /// Test Class for ThreeDivision.
    /// </summary>
    /// 
    [TestFixture]
    public class ThreeDivisionTests
    {
        /// <summary>
        /// Local Variable for Three Division.
        /// </summary>
        private ThreeDivisionRule threeDivision;

        /// <summary>
        /// To Initialize the objects used in the test cases.
        /// </summary>
        [SetUp]
        public void Initialise()
        {
            this.threeDivision = new ThreeDivisionRule();
        }

        /// <summary>
        /// Test Case for Three Division. Pass -- Tuesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Tuesday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Fizz, this.threeDivision.DivisionProcess(3, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Division. Pass -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Wednesday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Wizz, this.threeDivision.DivisionProcess(3, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail -- Tuesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Tuesday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Wizz, this.threeDivision.DivisionProcess(3, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Wednesday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Fizz, this.threeDivision.DivisionProcess(3, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Pass  -- Tuesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Tuesday_Pass()
        {
            Assert.AreEqual("4", this.threeDivision.DivisionProcess(4, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Pass  -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Wednesday_Pass()
        {
            Assert.AreEqual("4", this.threeDivision.DivisionProcess(4, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Tuesday_Fail()
        {
            Assert.AreNotEqual("4", this.threeDivision.DivisionProcess(5, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Wednesday_Fail()
        {
            Assert.AreNotEqual("4", this.threeDivision.DivisionProcess(5, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// To Dispose the used Objects.
        /// </summary>
        [TearDown]
        public void Dispose()
        {
            this.threeDivision = null;
        }

    }
}
