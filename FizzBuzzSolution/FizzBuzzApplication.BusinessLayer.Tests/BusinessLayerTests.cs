﻿// ---------------------------------------------------------------------------
// <copyright file="BusinessLayerTests.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains the test cases for Custom Repository.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Moq;
    using NUnit.Framework;
    using FizzBuzzApplication.BusinessLayer;
    /// <summary>
    /// Test Class for BusinessLayer.
    /// </summary>
    [TestFixture]
    public class BusinessLayerTests
    {
        /// <summary>
        /// Local BusinessLayer object.
        /// </summary>
        private BusinessLayer businessLayer;

        /// <summary>
        /// Local Mock object for ThreeDivisible.
        /// </summary>
        private Mock<IDivisionRule> idivisibleRuleMock;

        /// <summary>
        /// Local Expected collection.
        /// </summary>
        private List<string> expectedCollection;

        /// <summary>
        /// To Initialize the used objects.
        /// </summary>
        [SetUp]
        public void IntializeObjects()
        {
            this.expectedCollection = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            this.idivisibleRuleMock = new Mock<IDivisionRule>();

            var threefivedivisible = new Mock<IDivisionRule>();
            threefivedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                .Returns((int input, DayOfWeek two) => (input % 15 == 0 ? "fizz buzz" : input.ToString(CultureInfo.CurrentCulture)));

            var fivedivisible = new Mock<IDivisionRule>();
            fivedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                 .Returns((int input, DayOfWeek two) => (input % 5 == 0 ? "buzz" : input.ToString(CultureInfo.CurrentCulture)));

            var threedivisible = new Mock<IDivisionRule>();
            threedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                .Returns((int input, DayOfWeek two) => (input % 3 == 0 ? "fizz" : input.ToString(CultureInfo.CurrentCulture)));

            var divisibles = new List<IDivisionRule>() { threefivedivisible.Object, fivedivisible.Object, threedivisible.Object };
            this.businessLayer = new BusinessLayer(divisibles);
        }

        /// <summary>
        /// Test case to test Constructor. Pass
        /// </summary>
        [Test]
        public void Test_BusinessLayer_Constructor_Pass()
        {
            Assert.IsNotNull(this.businessLayer);
        }

        /// <summary>
        /// Test case to test Constructor. Fail
        /// </summary>
        [Test]
        public void Test_BusinessLayer_Constructor_Fail()
        {
            Assert.IsNotInstanceOf<ThreeDivisionRule>(this.businessLayer);
        }

        /// <summary>
        /// Get FizzBuzzCollection on Monday Pass.
        /// </summary>
        [Test]
        public void Test_GenerateFizzBuzzSequence_Monday_Count_Pass()
        {
            this.businessLayer.CurrentDay = DayOfWeek.Monday;
            Assert.AreEqual(this.expectedCollection.Count, this.businessLayer.GenerateFizzBuzzSequence(5).Count);
        }

        /// <summary>
        /// Get FizzBuzzCollection on Monday Fail.
        /// </summary>
        [Test]
        public void Test_GenerateFizzBuzzSequence_Monday_Count_Fail()
        {
            this.businessLayer.CurrentDay = DayOfWeek.Monday;
            Assert.AreNotEqual(this.expectedCollection.Count, this.businessLayer.GenerateFizzBuzzSequence(4).Count);
        }

        /// <summary>
        /// Test Case for FizzBuzzCollection on Monday Pass. 
        /// </summary>
        [Test]
        public void Test_GenerateFizzBuzzSequence_Monday_Collection_Pass()
        {
            this.businessLayer.CurrentDay = DayOfWeek.Monday;
            Assert.AreEqual(this.expectedCollection, this.businessLayer.GenerateFizzBuzzSequence(5));
        }

        /// <summary>
        /// Test Case for FizzBuzzCollection on Monday Fail. 
        /// </summary>
        [Test]
        public void Test_GenerateFizzBuzzSequence_Monday_Collection_Fail()
        {

            var threefivedivisible = new Mock<IDivisionRule>();
            threefivedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                .Returns((int input, DayOfWeek two) => (input % 15 == 0 ? "wizz wuzz" : input.ToString(CultureInfo.CurrentCulture)));

            var fivedivisible = new Mock<IDivisionRule>();
            fivedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                 .Returns((int input, DayOfWeek two) => (input % 5 == 0 ? "wuzz" : input.ToString(CultureInfo.CurrentCulture)));

            var threedivisible = new Mock<IDivisionRule>();
            threedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                .Returns((int input, DayOfWeek two) => (input % 3 == 0 ? "wizz" : input.ToString(CultureInfo.CurrentCulture)));


            var divisibles = new List<IDivisionRule>() { threefivedivisible.Object, fivedivisible.Object, threedivisible.Object };
            this.businessLayer = new BusinessLayer(divisibles);
            this.businessLayer.CurrentDay = DayOfWeek.Monday;

            Assert.AreNotEqual(this.expectedCollection, this.businessLayer.GenerateFizzBuzzSequence(5));
        }

        /// <summary>
        /// Get FizzBuzzCollection on Wednesday Pass.
        /// </summary>
        [Test]
        public void Test_GenerateFizzBuzzSequence_Wednesday_Collection_Pass()
        {
            this.expectedCollection = new List<string>() { "1", "2", "wizz", "4", "wuzz" };
            var threefivedivisible = new Mock<IDivisionRule>();
            threefivedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                .Returns((int input, DayOfWeek two) => (input % 15 == 0 ? "wizz wuzz" : input.ToString(CultureInfo.CurrentCulture)));

            var fivedivisible = new Mock<IDivisionRule>();
            fivedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                 .Returns((int input, DayOfWeek two) => (input % 5 == 0 ? "wuzz" : input.ToString(CultureInfo.CurrentCulture)));

            var threedivisible = new Mock<IDivisionRule>();
            threedivisible.Setup(m => m.DivisionProcess(It.IsAny<int>(), It.IsAny<DayOfWeek>()))
                .Returns((int input, DayOfWeek two) => (input % 3 == 0 ? "wizz" : input.ToString(CultureInfo.CurrentCulture)));


            var divisibles = new List<IDivisionRule>() { threefivedivisible.Object, fivedivisible.Object, threedivisible.Object };
            this.businessLayer = new BusinessLayer(divisibles);
            this.businessLayer.CurrentDay = DayOfWeek.Wednesday;
            Assert.AreEqual(this.expectedCollection, this.businessLayer.GenerateFizzBuzzSequence(5));
        }

        /// <summary>
        /// Get FizzBuzzCollection on Wednesday Pass.
        /// </summary>
        [Test]
        public void Test_GenerateFizzBuzzSequence_Wednesday_Collection__Fail()
        {
            this.expectedCollection = new List<string>() { "1", "2", "wizz", "4", "wuzz" };
            this.businessLayer.CurrentDay = DayOfWeek.Wednesday;
            Assert.AreNotEqual(this.expectedCollection, this.businessLayer.GenerateFizzBuzzSequence(5));
        }
        /// <summary>
        /// To Dispose the used Objects.
        /// </summary>
        [TearDown]
        public void Dispose()
        {
            this.businessLayer = null;
            this.expectedCollection = null;
            this.idivisibleRuleMock = null;
        }
    }
}
