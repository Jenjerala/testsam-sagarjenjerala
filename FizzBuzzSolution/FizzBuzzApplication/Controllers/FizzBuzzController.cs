﻿// ---------------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains FizzBuzzController class.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.Controllers
{
    using System.Web.Mvc;
    using BusinessLayer;
    using Models;
    using System.Configuration;
    using System;
    using PagedList;
    /// <summary>
    /// FizzBuzzController 
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// private BusinessLayer interface
        /// </summary>
        private readonly IBusinessLayer _businessLayer;

        /// <summary>
        /// page size
        /// </summary>
        private int PageSize
        {
            get
            {
                var pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                if (pageSize <= 0)
                    pageSize = 20;
                return pageSize;
            }
        }

        /// <summary>
        /// used to store the input value
        /// </summary>
        private int Input
        {
            get { return Convert.ToInt32(TempData["Input"]); }
            set { TempData["Input"] = value; }
        }

        /// <summary>
        /// FizzBuzzController constructor 
        /// </summary>
        /// <param name="businessLayer">businessLayer object</param>
        public FizzBuzzController(IBusinessLayer businessLayer)
        {
            _businessLayer = businessLayer;
        }
        //
        // GET: /FizzBuzz/

        /// <summary>
        /// used to return the FizzBuzz Home page
        /// </summary>
        /// <returns>FizzBuzzHome View</returns>
        [HttpGet]
        public ActionResult FizzBuzzHome(int? pageNumber)
        {
            FizzBuzzModel model = new FizzBuzzModel();
            if (ModelState.IsValid)
            {
                if (Input > 0)
                {
                    model = FetchFizzBuzzSequence(Input, pageNumber);
                }
            }
            return View("FizzBuzzHome", model);
        }

        /// <summary>
        /// used to return the generated FizzBuzz sequence
        /// </summary>
        /// <param name="model">FizzBuzz model</param>
        /// <returns>FizzBuzzHome View</returns>
        [HttpPost]
        public ActionResult FizzBuzzHome(FizzBuzzModel model, int? pageNumber)
        {
            if (ModelState.IsValid)
            {
                model = FetchFizzBuzzSequence(model.Input.Value, pageNumber);
                ModelState.Clear();
            }
            return View("FizzBuzzHome", model);
        }

        /// <summary>
        /// method to call BusinessLayer
        /// </summary>
        /// <param name="inputNumber">input number</param>
        /// <param name="pageNumber">page number</param>
        /// <returns>FizzBuzzModel</returns>
        private FizzBuzzModel FetchFizzBuzzSequence(int? inputNumber, int? pageNumber)
        {
            var fizzBuzzList = this._businessLayer.GenerateFizzBuzzSequence(inputNumber.Value);
            var pagenumber = pageNumber ?? 1;
            Input = inputNumber.Value;
            var model = new FizzBuzzModel { Input = inputNumber.Value, FizzBuzzList = fizzBuzzList.ToPagedList(pagenumber, PageSize) };
            return model;
        }
    }
}
